var btn = document.querySelector('button');
var canvas = document.querySelector('canvas');
var svgObject = document.getElementById('svg-object');
var claimElem = document.getElementById("claim");
var backgroundColor = document.getElementById("background-color");
var badgeType = document.getElementById("badge-type");
var scale = 1;
var svg;

function updateBadge() {
    var svgDoc = svgObject.contentDocument;
    svg = svgDoc.getElementById('svg-badge');
    var claim = claimElem.value;
    console.log(claim);
    var claimSvgElem = svgDoc.getElementById("claim-svg")
    claimSvgElem.textContent = claim;

    var color = backgroundColor.value;
    var rectSvgElem = svgDoc.getElementById("rect")
    rectSvgElem.style.color = color;
    rectSvgElem.style.fill = color;

    var pngWidth = document.getElementById("png-width").value;
    svgObject.width = pngWidth;
    scale = pngWidth / 564;

    canvas.width = 564 * scale;
    canvas.height = 168* scale;
    var ctx = canvas.getContext('2d');
    var data = (new XMLSerializer()).serializeToString(svg);
    var DOMURL = window.URL || window.webkitURL || window;
    var img = new Image();
    var svgBlob = new Blob([data], {type: 'image/svg+xml;charset=utf-8'});
    var url = DOMURL.createObjectURL(svgBlob);

    img.onload = function () {
        ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
        DOMURL.revokeObjectURL(url);

        var imgURI = canvas
          .toDataURL('image/png')
          .replace('image/png', 'image/octet-stream');
        };

        img.src = url;

        var svgUrl = URL.createObjectURL(svgBlob);
          //set url value to a element's href attribute.
          document.getElementById("svglink").href = svgUrl;
        }        

        btn.addEventListener('click', function () {
            updateBadge();
      });

badgeType.addEventListener('change', function() {
    var file = badgeType.value;
    if (file === 'get-it-on-blue-on-white.svg') {
        backgroundColor.value = '#ffffff';
    } else {
        backgroundColor.value = '#000000';
    }
    document.getElementById('svg-object').data = file;
    const context = canvas.getContext('2d');
    context.clearRect(0, 0, canvas.width, canvas.height);
})

function downloadPng() {
    var img    = canvas.toDataURL("image/png");
    document.write('<img src="'+img+'"/>');
}
function downloadSvg() {
    var svgDoc = svgObject.contentDocument;
    var svgBadge = svgDoc.getElementById('svg-badge');
    document.write(svgBadge);
}