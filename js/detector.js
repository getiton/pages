var theLang = navigator.language || navigator.userLanguage;
var lang;
if ( theLang == "es" ) {
    /* SPANISH */
    lang = "es";
} else if ( theLang == "es-419" ) {
    lang = "es";
} else if (theLang=="es-AR") {
    lang = "es";
} else if (theLang=="es-BO") {
    lang = "es";
} else if (theLang=="es-CL") {
    lang = "es";
} else if (theLang=="es-CO") {
    lang = "es";
} else if (theLang=="es-CR") {
    lang = "es";
} else if (theLang=="es-DO") {
    lang = "es";
} else if (theLang=="es-EC") {
    lang = "es";
} else if (theLang=="es-ES") {
    lang = "es";
} else if (theLang=="es-GT") {
    lang = "es";
} else if (theLang=="es-HN") {
    lang = "es";
} else if (theLang=="es-MX") {
    lang = "es";
} else if (theLang=="es-NI") {
    lang = "es";
} else if (theLang=="es-PA") {
    lang = "es";
} else if (theLang=="es-PE") {
    lang = "es";
} else if (theLang=="es-PR") {
    lang = "es";
} else if (theLang=="es-PY") {
    lang = "es";
} else if (theLang=="es-SV") {
    lang = "es";
} else if (theLang=="es-UY") {
    lang = "es";
} else if (theLang=="es-VE") {
    lang = "es";
} else {
    /* ENGLISH */
    lang = "en";
}
// document.write(theLang);